<?php

   if(isset($_POST["Enviar"])){
     include("conexion.php");

     $user=htmlentities(addslashes($_POST['user']));
     $password=htmlentities(addslashes($_POST['password']));

     $query=$conexion->prepare("SELECT * FROM usuarios WHERE user= :user AND password= :password");
     $query->bindValue(":user", $user);
     $query->bindValue(":password", $password);
     $query->execute();
     

     if($query->rowCount()!=0){
        while($row=$query->fetch()){
          $_SESSION['id']=$row['id'];
          $_SESSION['user']=$row['user'];
          $_SESSION['password']=$row['password'];
          $_SESSION['nombre']=$row['nombre'];
          $_SESSION['apellido_paterno']=$row['apellido_paterno'];
          $_SESSION['apellido_materno']=$row['apellido_materno'];
          $_SESSION['telefono']=$row['telefono'];
          $_SESSION['mail']=$row['mail'];
          $_SESSION['carrera']=$row['carrera'];
        }
        header("Location: main_page.php");
     }else{
       $_SESSION['message']='¡No existe Usuario, verifique sus datos!';
       $_SESSION['message_type']='danger';
       header("Location: Index.php");
     }
   }

 ?>
