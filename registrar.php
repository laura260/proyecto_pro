<?php

include("conexion.php");

if(isset($_POST['registro'])){

  $user=$_POST['user'];
  $password=$_POST['password'];
  $nombre=$_POST['nombre'];
  $apellido_materno=$_POST['apellido_materno'];
  $apellido_paterno=$_POST['apellido_paterno'];
  $telefono=$_POST['telefono'];
  $mail=$_POST['mail'];
  $carrera=$_POST['carrera'];

  


  $query=$conexion->prepare("INSERT INTO usuarios(user, password, nombre, apellido_paterno, apellido_materno, telefono, mail, carrera) VALUES(:user, :password, :nombre, :apellido_paterno, :apellido_materno, :telefono, :mail, :carrera)");
  $query->bindParam(':user', $user);
  $query->bindParam(':password', $password);
  $query->bindParam(':nombre', $nombre);
  $query->bindParam(':apellido_paterno', $apellido_paterno);
  $query->bindParam(':apellido_materno', $apellido_materno);
  $query->bindParam(':telefono', $telefono);
  $query->bindParam(':mail', $mail);
  $query->bindParam(':carrera', $carrera);
  $query->execute();

  if(!$query){
    $_SESSION['message']='¡Fallo al registrar!';
    $_SESSION['message_type']='danger';
  }else{
    $_SESSION['message']='¡Registro guardado con éxito!';
    $_SESSION['message_type']='succes';
  }
    header("Location: Register.php");
}

