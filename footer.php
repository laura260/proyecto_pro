
	<footer id="contacto" class="container-fluid bg-footer">
		<div class="container06">
			<ul>
				<a href="https://www.facebook.com/">
					<li><i class="fab fa-facebook fa-2x"></i></li>
				</a>
				<a href="https://twitter.com/login?lang=es">
					<li><i class="fab fa-twitter fa-2x"></i></li>
				</a>
				<a href="https://www.instagram.com/?hl=es-la">
					<li><i class="fab fa-instagram fa-2x"></i></li>
				</a>
			</ul>
		</div>
		<div class=" mx-auto container">
			<p style="color:#F2F2F2;">CENTRO UNIVERSITARIO DE TONALÁ
				Campus CUTonalá Av. Nuevo Periférico No. 555 Ejido San José Tatepozco, C.P. 45425, Tonalá Jalisco,
				México</p>
		</div>
		<div class="container07 container">
			<p>&copy; 2020 CUT</p><b style="font-family: 'Permanent Marker', cursive;">libros</b>
		</div>
	</footer>
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>

</html>