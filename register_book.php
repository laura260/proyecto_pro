<?php include("head.php") ?>

<div class="container-fluid">
    <div class="row mt-4">
        <form id="register_form" class="bg-form mx-auto col-xs-12 col-sm-5 col-md-5 form-c" action="reg_libro.php" method="POST" enctype="multipart/form-data">
            <?php if (isset($_SESSION['message'])) { ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <?= $_SESSION['message'] ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php session_unset();
            } ?>
            <div class="form-group">
                <label style="font-family: 'Didact Gothic', sans-serif; font-size: 28px;">Registro de Libros</label><br>
            </div>
            <div class="form-group">
                <input class="form-control" type="text" id="titulo" placeholder="Titulo" name="titulo" required>
                <br>
                <input class="form-control" type="text" id="autor" placeholder="Autor" name="autor" required>
                <br>
                <input class="form-control" type="text" id="editorial" placeholder="Editorial" name="editorial" required>
                <br>
                <label for="option" style="font-family: 'Didact Gothic', sans-serif; font-size: 22px;">Temática</label>
                <select class="form-control" id="tematica_option" name="tematica">
                    <option>Literatura</option>
                    <option>Cómic/Fantasía</option>
                    <option>Ciencia/Conocimiento</option>
                    <option>Gastronomía</option>
                    <option>Manuales</option>
                    <option>Biografías</option>
                </select>
                <br>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">$</span>
                    </div>
                    <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" placeholder="costo" name='precio'>
                    <div class="input-group-append">
                        <span class="input-group-text">.00</span>
                    </div>
                </div>
                <br>
            </div>
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile" name='imagen'>
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
            </div>
            <br>
            <input type="submit" name="registro_libro" class="btn btn-success btn-block" value="Registrar libro">

        </form>

    </div>
</div>
<br>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
<?php include("footer.php") ?>
