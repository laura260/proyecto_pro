<?php include("head.php") ?>

<div class="container-fluid">
    <div class="row mt-4">
        <form id="register_form" class="bg-form mx-auto col-xs-12 col-sm-5 col-md-5 form-c bg-dark text-white" action="registrar.php" method="POST" onsubmit="return validate()">
            <?php if (isset($_SESSION['message'])) { ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <?= $_SESSION['message'] ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php session_unset();
            } ?>
            <div class="form-group">
                <label style="font-family: 'Didact Gothic', sans-serif; font-size: 28px;">Titulo</label><br>
            </div>
            <div class="form-group">
                <img class="imagen4" src="imagenes/portada1.jpg">
            </div>
            <div class="form-group">
                <br>
                <label style="font-family: 'Didact Gothic', sans-serif; font-size: 28px;">Autor</label><br>
                <br>
                <label style="font-family: 'Didact Gothic', sans-serif; font-size: 28px;">Editorial</label><br>
                <br>
                <label style="font-family: 'Didact Gothic', sans-serif; font-size: 28px;">Precio</label><br>
                <br>
                <label style="font-family: 'Didact Gothic', sans-serif; font-size: 28px;">Temática</label><br>
                <br>
            </div>
            <div class="form-group">
                <br>
                <label style="font-family: 'Didact Gothic', sans-serif; font-size: 28px;">Contacto</label><br>
                <br>
                <label style="font-family: 'Didact Gothic', sans-serif; font-size: 28px;">Telefono</label><br>
                <br>
            </div>
        </form>

    </div>
</div>
<br>
<?php include("footer.php") ?>