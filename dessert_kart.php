<?php

include('conexion.php');

include('dessert_search.php');

$insertHelado=false;
$insertCheescake=false;
$insertCrepa=false;
$insertPastel=false;
$insertRed=false;
$insertTiramisu=false;
$insertFlan=false;
$insertBrownie=false;
$insertMuffin=false;


if(!isset($_SESSION['cantidadHelado'])){
    $_SESSION['cantidadHelado']=0;
    $insertHelado=true;
}if (!isset($_SESSION['cantidadCheescake'])) {
  $_SESSION['cantidadCheescake']=0;
  $insertCheescake=true;
}if (!isset($_SESSION['cantidadPastel'])) {
  $_SESSION['cantidadPastel']=0;
  $insertPastel=true;
}if (!isset($_SESSION['cantidadCrepa'])) {
  $_SESSION['cantidadCrepa']=0;
  $insertCrepa=true;
}if (!isset($_SESSION['cantidadRed'])) {
  $_SESSION['cantidadRed']=0;
  $insertRed=true;
}if (!isset($_SESSION['cantidadTiramisu'])) {
    $_SESSION['cantidadTiramisu']=0;
    $insertTiramisu=true;
}if (!isset($_SESSION['cantidadFlan'])) {
      $_SESSION['cantidadFlan']=0;
      $insertFlan=true;
}if (!isset($_SESSION['cantidadBrownie'])) {
  $_SESSION['cantidadBrownie']=0;
  $insertBrownie=true;
}if (!isset($_SESSION['cantidadMuffin'])) {
  $_SESSION['cantidadMuffin']=0;
  $insertMuffin=true;
}

if (isset($_POST['btnhelado'])) {

    $_SESSION['cantidadHelado']+=$_POST['quantity_helado'];
    $_SESSION['totalHelado']=$_SESSION['cantidadHelado']*$_SESSION['helado'];

    if($insertHelado==true){
      $query=$conexion->prepare("INSERT INTO pedidos(name, product_image, product_name, quantity, unitary_price, total_price) VALUES(:name, 'imagenes/helado.jpg', 'Helado', :cantidad, :precio, :precio_total)");
      $query->bindParam(':name', $_SESSION['nameP']);
      $query->bindParam(':cantidad', $_SESSION['cantidadHelado']);
      $query->bindParam(':precio', $_SESSION['helado']);
      $query->bindParam(':precio_total', $_SESSION['totalHelado']);
      $query->execute();
    }else{
      $query=$conexion->prepare("UPDATE pedidos set name=:name, product_image='imagenes/helado.jpg', product_name='Helado', quantity=:cantidad, unitary_price=:precio, total_price=:precio_total where product_name='Helado'");
      $query->bindParam(':name', $_SESSION['nameP']);
      $query->bindParam(':cantidad', $_SESSION['cantidadHelado']);
      $query->bindParam(':precio', $_SESSION['helado']);
      $query->bindParam(':precio_total', $_SESSION['totalHelado']);
      $query->execute();
    }


}if(isset($_POST['btncheescake'])) {

    $_SESSION['cantidadCheescake']+=$_POST['quantity_cheescake'];
    $_SESSION['totalCheescake']=$_SESSION['cantidadCheescake']*$_SESSION['cheescake'];

    if($insertCheescake==true){
      $query2=$conexion->prepare("INSERT INTO pedidos(name, product_image, product_name, quantity, unitary_price, total_price) VALUES(:name, 'imagenes/cheescake.jpg', 'Cheescake', :cantidad, :precio, :precio_total)");
      $query2->bindParam(':name', $_SESSION['nameP']);
      $query2->bindParam(':cantidad', $_SESSION['cantidadCheescake']);
      $query2->bindParam(':precio', $_SESSION['cheescake']);
      $query2->bindParam(':precio_total', $_SESSION['totalCheescake']);
      $query2->execute();

    }else{
      $query2=$conexion->prepare("UPDATE pedidos set name=:name, product_image='imagenes/cheescake.jpg', product_name='Cheescake', quantity=:cantidad, unitary_price=:precio, total_price=:precio_total where product_name='Cheescake'");
      $query2->bindParam(':name', $_SESSION['nameP']);
      $query2->bindParam(':cantidad', $_SESSION['cantidadCheescake']);
      $query2->bindParam(':precio', $_SESSION['cheescake']);
      $query2->bindParam(':precio_total', $_SESSION['totalCheescake']);
      $query2->execute();

    }

}if(isset($_POST['btnpastel'])) {

    $_SESSION['cantidadPastel']+=$_POST['quantity_pastel'];
    $_SESSION['totalPastel']=$_SESSION['cantidadPastel']*$_SESSION['pastel'];

    if($insertPastel==true){
      $query3=$conexion->prepare("INSERT INTO pedidos(name, product_image, product_name, quantity, unitary_price, total_price) VALUES(:name, 'imagenes/pastel-manzana.jpg', 'Pastel de Manzana', :cantidad, :precio, :precio_total)");
      $query3->bindParam(':name', $_SESSION['nameP']);
      $query3->bindParam(':cantidad', $_SESSION['cantidadPastel']);
      $query3->bindParam(':precio', $_SESSION['pastel']);
      $query3->bindParam(':precio_total', $_SESSION['totalPastel']);
      $query3->execute();
    }else{
      $query3=$conexion->prepare("UPDATE pedidos set name=:name, product_image='imagenes/pastel-manzana.jpg', product_name='Pastel de Manzana', quantity=:cantidad, unitary_price=:precio, total_price=:precio_total where product_name='Pastel de Manzana'");
      $query3->bindParam(':name', $_SESSION['nameP']);
      $query3->bindParam(':cantidad', $_SESSION['cantidadPastel']);
      $query3->bindParam(':precio', $_SESSION['pastel']);
      $query3->bindParam(':precio_total', $_SESSION['totalPastel']);
      $query3->execute();
    }

}if(isset($_POST['btncrepa'])) {

    $_SESSION['cantidadCrepa']+=$_POST['quantity_crepa'];
    $_SESSION['totalCrepa']=$_SESSION['cantidadCrepa']*$_SESSION['crepa'];

    if($insertCrepa==true){
      $query4=$conexion->prepare("INSERT INTO pedidos(name, product_image, product_name, quantity, unitary_price, total_price) VALUES(:name, 'imagenes/crepas.jpg', 'Crepas', :cantidad, :precio, :precio_total)");
      $query4->bindParam(':name', $_SESSION['nameP']);
      $query4->bindParam(':cantidad', $_SESSION['cantidadCrepa']);
      $query4->bindParam(':precio', $_SESSION['crepa']);
      $query4->bindParam(':precio_total', $_SESSION['totalCrepa']);
      $query4->execute();
    }else{
      $query4=$conexion->prepare("UPDATE pedidos set name=:name, product_image='imagenes/crepas.jpg', product_name='Crepas', quantity=:cantidad, unitary_price=:precio, total_price=:precio_total where product_name='Crepas'");
      $query4->bindParam(':name', $_SESSION['nameP']);
      $query4->bindParam(':cantidad', $_SESSION['cantidadCrepa']);
      $query4->bindParam(':precio', $_SESSION['crepa']);
      $query4->bindParam(':precio_total', $_SESSION['totalCrepa']);
      $query4->execute();
    }

}if(isset($_POST['btnred'])) {

    $_SESSION['cantidadRed']+=$_POST['quantity_red'];
    $_SESSION['totalRed']=$_SESSION['cantidadRed']*$_SESSION['red'];

    if($insertRed==true){
      $query5=$conexion->prepare("INSERT INTO pedidos(name, product_image, product_name, quantity, unitary_price, total_price) VALUES(:name, 'imagenes/red-velvet.jpg', 'Red Velvet', :cantidad, :precio, :precio_total)");
      $query5->bindParam(':name', $_SESSION['nameP']);
      $query5->bindParam(':cantidad', $_SESSION['cantidadRed']);
      $query5->bindParam(':precio', $_SESSION['red']);
      $query5->bindParam(':precio_total', $_SESSION['totalRed']);
      $query5->execute();
    }else{
      $query5=$conexion->prepare("UPDATE pedidos set name=:name, product_image='imagenes/red-velvet.jpg', product_name='Red Velvet', quantity=:cantidad, unitary_price=:precio, total_price=:precio_total where product_name='Red Velvet'");
      $query5->bindParam(':name', $_SESSION['nameP']);
      $query5->bindParam(':cantidad', $_SESSION['cantidadRed']);
      $query5->bindParam(':precio', $_SESSION['red']);
      $query5->bindParam(':precio_total', $_SESSION['totalRed']);
      $query5->execute();
    }



}if(isset($_POST['btntiramisu'])) {

    $_SESSION['cantidadTiramisu']+=$_POST['quantity_tiramisu'];
    $_SESSION['totalTiramisu']=$_SESSION['cantidadTiramisu']*$_SESSION['tiramisu'];

    if($insertTiramisu==true){
      $query6=$conexion->prepare("INSERT INTO pedidos(name, product_image, product_name, quantity, unitary_price, total_price) VALUES(:name, 'imagenes/tiramisu.jpg', 'Tiramisu', :cantidad, :precio, :precio_total)");
      $query6->bindParam(':name', $_SESSION['nameP']);
      $query6->bindParam(':cantidad', $_SESSION['cantidadTiramisu']);
      $query6->bindParam(':precio', $_SESSION['tiramisu']);
      $query6->bindParam(':precio_total', $_SESSION['totalTiramisu']);
      $query6->execute();
    }else{
      $query6=$conexion->prepare("UPDATE pedidos set name=:name, product_image='imagenes/tiramisu.jpg', product_name='Tiramisu', quantity=:cantidad, unitary_price=:precio, total_price=:precio_total where product_name='Tiramisu'");
      $query6->bindParam(':name', $_SESSION['nameP']);
      $query6->bindParam(':cantidad', $_SESSION['cantidadTiramisu']);
      $query6->bindParam(':precio', $_SESSION['tiramisu']);
      $query6->bindParam(':precio_total', $_SESSION['totalTiramisu']);
      $query6->execute();
    }

}if(isset($_POST['btnflan'])) {

    $_SESSION['cantidadFlan']+=$_POST['quantity_flan'];
    $_SESSION['totalFlan']=$_SESSION['totalFlan']*$_SESSION['flan'];

    if($insertFlan==true){
      $query=$conexion->prepare("INSERT INTO pedidos(name, product_image, product_name, quantity, unitary_price, total_price) VALUES(:name, 'imagenes/flan.jpg', 'Flan', :cantidad, :precio, :precio_total)");
      $query7->bindParam(':name', $_SESSION['nameP']);
      $query7->bindParam(':cantidad', $_SESSION['cantidadFlan']);
      $query7->bindParam(':precio', $_SESSION['flan']);
      $query7->bindParam(':precio_total', $_SESSION['totalFlan']);
      $query7->execute();
    }else{
      $query7=$conexion->prepare("UPDATE pedidos set name=:name, product_image='imagenes/flan.jpg', product_name='Flan', quantity=:cantidad, unitary_price=:precio, total_price=:precio_total where product_name='Flan'");
      $query7->bindParam(':name', $_SESSION['nameP']);
      $query7->bindParam(':cantidad', $_SESSION['cantidadFlan']);
      $query7->bindParam(':precio', $_SESSION['flan']);
      $query7->bindParam(':precio_total', $_SESSION['totalFlan']);
      $query7->execute();
    }

}if(isset($_POST['btnmuffin'])) {

    $_SESSION['cantidadMuffin']+=$_POST['quantity_muffin'];
    $_SESSION['totalMuffin']=$_SESSION['cantidadMuffin']*$_SESSION['muffin'];

    if($insertMuffin==true){
      $query8=$conexion->prepare("INSERT INTO pedidos(name, product_image, product_name, quantity, unitary_price, total_price) VALUES(:name, 'imagenes/muffins.jpg', 'Muffin', :cantidad, :precio, :precio_total)");
      $query8->bindParam(':name', $_SESSION['nameP']);
      $query8->bindParam(':cantidad', $_SESSION['cantidadMuffin']);
      $query8->bindParam(':precio', $_SESSION['muffin']);
      $query8->bindParam(':precio_total', $_SESSION['totalMuffin']);
      $query8->execute();
    }else{
      $query8=$conexion->prepare("UPDATE pedidos set name=:name, product_image='imagenes/muffins.jpg', product_name='Muffin', quantity=:cantidad, unitary_price=:precio, total_price=:precio_total where product_name='Muffin'");
      $query8->bindParam(':name', $_SESSION['nameP']);
      $query8->bindParam(':cantidad', $_SESSION['cantidadMuffin']);
      $query8->bindParam(':precio', $_SESSION['muffin']);
      $query8->bindParam(':precio_total', $_SESSION['totalMuffin']);
      $query8->execute();
    }

}if(isset($_POST['btnbrownie'])) {

    $_SESSION['cantidadBrownie']+=$_POST['quantity_brownie'];
    $_SESSION['totalBrownie']=$_SESSION['cantidadBrownie']*$_SESSION['brownie'];

    if($insertBrownie==true){
      $query=$conexion->prepare("INSERT INTO pedidos(name, product_image, product_name, quantity, unitary_price, total_price) VALUES(:name, 'imagenes/brownie.jpg', 'Brownie', :cantidad, :precio, :precio_total)");
      $query->bindParam(':name', $_SESSION['nameP']);
      $query->bindParam(':cantidad', $_SESSION['cantidadBrownie']);
      $query->bindParam(':precio', $_SESSION['brownie']);
      $query->bindParam(':precio_total', $_SESSION['totalBrownie']);
      $query->execute();
    }else{
      $query=$conexion->prepare("UPDATE pedidos set name=:name, product_image='imagenes/brownie.jpg', product_name='Brownie', quantity=:cantidad, unitary_price=:precio, total_price=:precio_total where product_name='Brownie'");
      $query->bindParam(':name', $_SESSION['nameP']);
      $query->bindParam(':cantidad', $_SESSION['cantidadBrownie']);
      $query->bindParam(':precio', $_SESSION['brownie']);
      $query->bindParam(':precio_total', $_SESSION['totalBrownie']);
      $query->execute();
    }

}
  $_SESSION['total_dessert']=$_SESSION['totalBrownie']+$_SESSION['totalMuffin']+$_SESSION['totalFlan']+$_SESSION['totalTiramisu']+$_SESSION['totalRed']+$_SESSION['totalCrepa']+$_SESSION['totalPastel']+$_SESSION['totalCheescake']+$_SESSION['totalHelado'];

   header("Location: carrito.php");



 ?>
