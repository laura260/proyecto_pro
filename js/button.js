$(function(){
    var valueElement = $('#quantity');
    function incrementValue(e){
        valueElement.text(Math.max(parseInt(valueElement.text()) + e.data.increment, 0));
        return false;
    }
    $('#plus').bind('click', {increment: 1}, incrementValue);
    $('#minus').bind('click', {increment: -1}, incrementValue);
});

/* $('#your-button').on("click", function(){
       var somevar1 = "your variable1";
       var somevar2 = "your variable2";
    $.ajax({
        type:"POST",
        url: "your-phpfile.php",
        data: "variable1=" + somevar1 + "\u0026variable2="+ somevar2,
        success: function(){
        //do stuff after the AJAX calls successfully completes
    }
    });
}); */