<!DOCTYPE html>
<html>

<head>
    <title>CUTlibros</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimun-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="icon" href="imagenes/book.ico">
    <link rel="stylesheet" type="text/css" href="css/EstiloIndex.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
    <script src="js/jquery.js"></script>
    <script src="js/jquery.mask.js"></script>

</head>

<body>

    
        <nav class="navbar navbar-expand-md bg-navbar navbar-font">
        <div class="container">
            <a href="main_page.php">
                <div class="container05"><i class="fas fa-book fa-1x" style="color: white; padding-top: 11px;"></i>
                    <p>CUT</p>
                    <p style="font-family: 'Permanent Marker', cursive;">libros</p>
                </div>
            </a>
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#nav1">
                <span class="navbar-toggler-icon">
                    <i style="color: white;" class="fas fa-book fa-1x"></i>
                </span>
            </button>
            <div class="collapse navbar-collapse" id="nav1">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#contacto">Contacto</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="register_book.php">Registrar libro</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="cerrarsesion.php">Cerrar sesión</a>
                    </li>

                </ul>
                <ul class="navbar-nav ml-auto">
                    <span class="navbar-text white-text font_span">
                        ¡Bienvenido:
                        <?php echo 'Barry';
                        ?>
                        <?php echo 'Garcia';
                        ?>!
                    </span>

                </ul>
            </div>
        </div>
        </nav>
    