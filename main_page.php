<?php include("head.php") ?>

<div class="container mt-4">
	<form class="" action="search.php" method="post" id="main_page">
		<div class="row">
			<div class="col-md-12">
				<div id="carousel" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carousel" data-slide-to="0" class="active"></li>
						<li data-target="#carousel" data-slide-to="1"></li>
						<li data-target="#carousel" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner" role="listbox">
						<div class="carousel-item active">
							<img src="imagenes/libroCarrusel6.jpg" class="img-responsive imagen2">
							<div class="carousel-caption bg-carousel">
								<h4 style="font-size: 28px;"><b>Literatura, ciencia, política</b></h4>
								<p style="font-size: 22px">y diversos temas más</p>
							</div>
						</div>
						<div class="carousel-item">
							<img src="imagenes/libroCarrusel4.jpg" class="img-responsive imagen2">
							<div class="carousel-caption bg-carousel">
								<h4 style="font-size: 28px;"><b>Información para tu tesis</b></h4>
								<p style="font-size: 22px">en un sólo lugar</p>
							</div>
						</div>
						<div class="carousel-item ">
							<img src="imagenes/libroCarrusel5.jpg" class="img-responsive imagen2">
							<div class="carousel-caption bg-carousel">
								<h4 style="font-size: 28px;"><b>Libros de especialidad</b></h4>
								<p style="font-size: 22px">más accesibles que nunca</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br>
		<?php include("book_search.php") ?>
		<?php	
		 foreach ($query as $row):?>
		<div class="row">
			<div class="col-md-12">
				<div class="row food-items ">
					<div class="">
						<img class="imagen5" src=<?php echo $row['imagen']?>>
					</div>
					<div class="book-container col-sm-12 col-md-8 ">
						<label style="font-family: 'Didact Gothic', sans-serif; font-size: 22px;">Titulo: <?php echo $row['titulo']?></label><br>
						<br>
						<label style="font-family: 'Didact Gothic', sans-serif; font-size: 22px;">Autor: <?php echo $row['autor']?></label><br>
						<br>
						<label style="font-family: 'Didact Gothic', sans-serif; font-size: 22px;">Editorial: <?php echo $row['editorial']?></label><br>
						<br>
						<label style="font-family: 'Didact Gothic', sans-serif; font-size: 22px;">Precio: <?php echo $row['precio']?> pesos</label><br>
						<br>
						<label style="font-family: 'Didact Gothic', sans-serif; font-size: 22px;">Temática: <?php echo $row['tematica']?></label><br>
						<br>
						<label style="font-family: 'Didact Gothic', sans-serif; font-size: 22px;">Contacto: <?php echo $row['nombre']?> <?php echo $row['apellido_p']?></label><br>
						<br>
						<label style="font-family: 'Didact Gothic', sans-serif; font-size: 22px;">Telefono: <?php echo $row['contacto']?></label><br>
					</div>
				</div>
			</div>
		</div>
		<br>
		<?php	
		 endforeach;
		 ?>
		

	</form>
</div>

<?php include("footer.php") ?>