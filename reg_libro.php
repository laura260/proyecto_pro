<?php

include("conexion.php");

if (isset($_POST['registro_libro'])) {

    $titulo = $_POST['titulo'];
    $autor = $_POST['autor'];
    $editorial = $_POST['editorial'];
    $tematica = $_POST['tematica'];
    $precio = $_POST['precio'];

    $imagen = $_FILES['imagen']['name'];
    $tipo_imagen = $_FILES['imagen']['size'];
    $carpeta_origen = $_FILES['imagen']['tmp_name'];

    $carpeta_destino = 'files/' . $imagen;
    move_uploaded_file($carpeta_origen, $carpeta_destino);



    $query = $conexion->prepare("INSERT INTO material(titulo, autor, editorial, tematica, precio, imagen, nombre, apellido_p, contacto) VALUES(:titulo, :autor, :editorial, :tematica, :precio, :imagen, :nombre, :apellido_p, :contacto)");
    $query->bindParam(':titulo', $titulo);
    $query->bindParam(':autor', $autor);
    $query->bindParam(':editorial', $editorial);
    $query->bindParam(':tematica', $tematica);
    $query->bindParam(':precio', $precio);
    $query->bindParam(':imagen', $carpeta_destino);
    $query->bindParam(':nombre', $_SESSION['nombre']);
    $query->bindParam(':apellido_p', $_SESSION['apellido_paterno']);
    $query->bindParam(':contacto', $_SESSION['telefono']);
    $query->execute();

    if (!$query) {
        $_SESSION['message'] = '¡Fallo al registrar!';
        $_SESSION['message_type'] = 'danger';
    } else {
        $_SESSION['message'] = '¡Registro guardado con éxito!';
        $_SESSION['message_type'] = 'succes';
    }
    header("Location: register_book.php");
}
